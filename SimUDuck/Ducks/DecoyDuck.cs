﻿using System;
using SimUDuck.FlyBehavior;
using SimUDuck.QuackBehavior;

namespace SimUDuck.Ducks
{
    public class DecoyDuck : Duck
    {
        public DecoyDuck()
            : base(new MuteQuack(), new FlyNoWay())
        {

        }


        public override void Display()
        {
            Console.WriteLine("Hello! I'm decoy duck.");
        }
    }
}