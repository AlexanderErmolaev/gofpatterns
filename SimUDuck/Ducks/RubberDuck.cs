﻿using System;
using SimUDuck.FlyBehavior;
using SimUDuck.QuackBehavior;

namespace SimUDuck.Ducks
{
    public class RubberDuck : Duck
    {
        public RubberDuck()
            : base(new Squeak(), new FlyNoWay())
        {

        }


        public override void Display()
        {
            Console.WriteLine("Hello! I'm rubber duck.");
        }
    }
}