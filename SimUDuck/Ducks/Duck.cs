﻿using System;
using SimUDuck.FlyBehavior;
using SimUDuck.QuackBehavior;

namespace SimUDuck.Ducks
{
    public abstract class Duck
    {
        public IQuackBehavior QuackBehavior { get; set; }

        public IFlyBehavior FlyBehavior { get; set; }


        protected Duck(IQuackBehavior quackBehavior, IFlyBehavior flyBehavior)
        {
            QuackBehavior = quackBehavior;
            FlyBehavior = flyBehavior;
        }


        public void PerfomQuack()
        {
            QuackBehavior.Quack();
        }

        public void PerfomFly()
        {
            FlyBehavior.Fly();
        }

        public static void Swim()
        {
            Console.WriteLine("I'm swimming!");
        }

        public abstract void Display();
    }
}