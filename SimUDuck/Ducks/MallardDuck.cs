﻿using System;
using SimUDuck.FlyBehavior;
using SimUDuck.QuackBehavior;

namespace SimUDuck.Ducks
{
    public class MallardDuck : Duck
    {
        public MallardDuck()
            : base(new OrdinaryQuack(), new FlyWithWings())
        {

        }


        public override void Display()
        {
            Console.WriteLine("Hello! I'm mallard duck.");
        }
    }
}