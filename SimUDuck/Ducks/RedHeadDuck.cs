﻿using System;
using SimUDuck.FlyBehavior;
using SimUDuck.QuackBehavior;

namespace SimUDuck.Ducks
{
    public class RedHeadDuck : Duck
    {
        public RedHeadDuck()
            : base(new OrdinaryQuack(), new FlyWithWings())
        {

        }


        public override void Display()
        {
            Console.WriteLine("Hello! I'm redhead duck.");
        }
    }
}