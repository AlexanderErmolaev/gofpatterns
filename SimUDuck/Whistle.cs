﻿using SimUDuck.QuackBehavior;

namespace SimUDuck
{
    public class Whistle
    {
        public IQuackBehavior QuackBehavior { get; set; }


        public Whistle()
        {
            QuackBehavior = new OrdinaryQuack();
        }


        public void Quack()
        {
            QuackBehavior.Quack();
        }
    }
}