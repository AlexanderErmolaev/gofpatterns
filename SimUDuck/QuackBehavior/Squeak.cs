﻿using System;

namespace SimUDuck.QuackBehavior
{
    public class Squeak : IQuackBehavior
    {
        public void Quack()
        {
            Console.WriteLine("I'm squeaking!");
        }
    }
}