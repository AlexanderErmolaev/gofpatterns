﻿using System;

namespace SimUDuck.QuackBehavior
{
    public class OrdinaryQuack : IQuackBehavior
    {
        public void Quack()
        {
            Console.WriteLine("I'm quacking!");
        }
    }
}