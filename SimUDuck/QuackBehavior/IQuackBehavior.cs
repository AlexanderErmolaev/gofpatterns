﻿namespace SimUDuck.QuackBehavior
{
    public interface IQuackBehavior
    {
        void Quack();
    }
}