﻿using System;

namespace SimUDuck.QuackBehavior
{
    public class MuteQuack : IQuackBehavior
    {
        public void Quack()
        {
            Console.WriteLine("I can't quack");
        }
    }
}