﻿using System;
using PizzaStore.Pizzas;

namespace PizzaStore
{
    public class ChicagoStylePizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string pizzaType)
        {
            IPizzaIngredientFactory pizzaIngredientFactory = new ChicagoPizzalngredientFactory();
            switch (pizzaType)
            {
                case "Cheese":
                    return new CheesePizza(pizzaIngredientFactory) { Name = "Chicago Style Cheese Pizza" };
                case "Pepperoni":
                    return new PepperoniPizza(pizzaIngredientFactory) { Name = "Chicago Style Cheese Pizza" };
                case "Clam":
                    return new ClamPizza(pizzaIngredientFactory) { Name = "Chicago Style Cheese Pizza" };
                case "Veggie":
                    return new VeggiePizza(pizzaIngredientFactory) { Name = "Chicago Style Cheese Pizza" };
                default:
                    throw new ArgumentOutOfRangeException("pizzaType");
            }
        }
    }
}