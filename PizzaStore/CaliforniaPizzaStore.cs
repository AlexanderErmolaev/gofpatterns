﻿using System;
using PizzaStore.Pizzas;

namespace PizzaStore
{
    public class CaliforniaPizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string pizzaType)
        {
            switch (pizzaType)
            {
                case "Cheese":
                    return new CaliforniaStyleCheesePizza();
                case "Pepperoni":
                    return new CaliforniaStylePepperoniPizza();
                case "Clam":
                    return new CaliforniaStyleClamPizza();
                case "Veggie":
                    return new CaliforniaStyleVeggiePizza();
                default:
                    throw new ArgumentOutOfRangeException("pizzaType");
            }
        }
    }
}