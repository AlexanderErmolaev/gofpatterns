﻿using System;
using PizzaStore.PizzaIngredients;

namespace PizzaStore.Pizzas
{
    public abstract class Pizza
    {
        protected Dough Dough;
        protected Sauce Sauce;
        protected Veggies[] Veggies;
        protected Cheese Cheese;
        protected Pepperoni Pepperoni;
        protected Clams Clam;


        public string Name { get; set; }


        public abstract void Prepare();

        public static void Bake()
        {
            Console.WriteLine("Bake for 25 minutes at 350.");
        }

        public virtual void Cut()
        {
            Console.WriteLine("Cutting the pizza into diagonal slices.");
        }

        public static void Box()
        {
            Console.WriteLine("Place pizza in offical PizzaStore box.");
        }
    }
}