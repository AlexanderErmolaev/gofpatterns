using System;

namespace PizzaStore.Pizzas
{
    public class CaliforniaStyleCheesePizza : Pizza
    {
        private readonly string _name;
        private readonly string _dough;
        private readonly string _sauce;
        private readonly string[] _toppings;


        public CaliforniaStyleCheesePizza()
        {
            _name = "NY Style Sauce and Cheese Pizza";
            _dough = "Thin Crust Dough";
            _sauce = "Marinara Sauce";
            _toppings = new[] { "Grated Reggiano Cheese" };
        }


        public override void Prepare()
        {
            Console.WriteLine("Preparing " + _name);
            Console.WriteLine(String.Format("Tossing {0} dough...", _dough));
            Console.WriteLine(String.Format("Tossing {0} sauce...", _sauce));
            Console.WriteLine("Adding toppings: ");
            foreach (var topping in _toppings)
            {
                Console.WriteLine(" " + topping);
            }
        }
    }
}