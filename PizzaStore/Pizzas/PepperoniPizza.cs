using System;

namespace PizzaStore.Pizzas
{
    public class PepperoniPizza : Pizza
    {
        readonly IPizzaIngredientFactory _ingredientFactory;


        public PepperoniPizza(IPizzaIngredientFactory ingredientFactory)
        {
            _ingredientFactory = ingredientFactory;
        }


        public override void Prepare()
        {
            Console.WriteLine("Preparing " + Name);
            Dough = _ingredientFactory.CreateDough();
            Sauce = _ingredientFactory.CreateSauce();
            Cheese = _ingredientFactory.CreateCheese();
        }
    }
}