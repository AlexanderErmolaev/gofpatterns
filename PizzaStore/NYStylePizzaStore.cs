﻿using System;
using PizzaStore.Pizzas;

namespace PizzaStore
{
    public class NyStylePizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string pizzaType)
        {
            IPizzaIngredientFactory pizzaIngredientFactory = new NyPizzalngredientFactory();
            switch (pizzaType)
            {
                case "Cheese":
                    return new CheesePizza(pizzaIngredientFactory) { Name = "New York Style Cheese Pizza" };
                case "Pepperoni":
                    return new PepperoniPizza(pizzaIngredientFactory) { Name = "New York Style Pepperoni Pizza" };
                case "Clam":
                    return new ClamPizza(pizzaIngredientFactory) { Name = "New York Style Clam Pizza" };
                case "Veggie":
                    return new VeggiePizza(pizzaIngredientFactory) { Name = "New York Style Veggie Pizza" };
                default:
                    throw new ArgumentOutOfRangeException("pizzaType");
            }
        }
    }
}