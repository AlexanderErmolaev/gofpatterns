﻿using PizzaStore.Pizzas;

namespace PizzaStore
{
    public abstract class PizzaStore
    {
        public Pizza OrderPizza(string pizzaType)
        {
            var pizza = CreatePizza(pizzaType);

            pizza.Prepare();
            Pizza.Bake();
            pizza.Cut();
            Pizza.Box();

            return pizza;
        }

        protected abstract Pizza CreatePizza(string type);
    }
}