﻿using System;
using SimUDuck;
using SimUDuck.Ducks;
using SimUDuck.FlyBehavior;
using SimUDuck.QuackBehavior;

namespace SimUDuckConsole
{
    static class Program
    {
        static void Main()
        {
            var ducks = new Duck[] { new RedHeadDuck(), new MallardDuck(), new RubberDuck(), new DecoyDuck() };

            foreach (var duck in ducks)
            {
                duck.Display();
                Duck.Swim();
                duck.PerfomQuack();
                duck.PerfomFly();
            }

            Duck mallardDuck = new MallardDuck();
            mallardDuck.Display();
            Duck.Swim();
            mallardDuck.PerfomQuack();
            mallardDuck.PerfomFly();
            mallardDuck.FlyBehavior = new FlyNoWay();
            mallardDuck.QuackBehavior = new MuteQuack();
            mallardDuck.PerfomQuack();
            mallardDuck.PerfomFly();

            var whistle = new Whistle();
            whistle.Quack();
            whistle.Quack();
            whistle.Quack();

            Console.ReadKey();
        }
    }
}