﻿using System;
using WeatherStation;
using WeatherStation.Displays;

namespace WeatherStationConsole
{
    static class Program
    {
        static void Main()
        {
            var weatherData = new WeatherData();

            var displayElements = new IDisplayElement[] { new CurrentConditionsDisplay(weatherData), new ForecastDisplay(weatherData), new StatisticsDisplay(weatherData) };

            foreach (var displayElement in displayElements)
            {
                displayElement.Display();
            }

            weatherData.SetMeasurements(80, 65, 305);
            weatherData.SetMeasurements(80, 65, 305);
            weatherData.SetMeasurements(80, 65, 305);

            Console.ReadKey();
        }
    }
}