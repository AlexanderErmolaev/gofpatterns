﻿namespace RemoteController
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }
}