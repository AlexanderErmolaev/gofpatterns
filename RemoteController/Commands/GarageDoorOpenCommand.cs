﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class GarageDoorOpenCommand : ICommand
    {
        readonly GarageDoor _garageDoor;


        public GarageDoorOpenCommand(GarageDoor door)
        {
            _garageDoor = door;
        }


        public void Execute()
        {
            _garageDoor.Open();
        }

        public void Undo()
        {
            _garageDoor.Close();
        }
    }
}