﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class TvOffCommand : ICommand
    {
        readonly TV _tv;


        public TvOffCommand(TV tv)
        {
            _tv = tv;
        }


        public void Execute()
        {
            _tv.Off();
        }

        public void Undo()
        {
            _tv.On();
        }
    }
}