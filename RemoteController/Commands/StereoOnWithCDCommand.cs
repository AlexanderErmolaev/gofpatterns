﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class StereoOnWithCdCommand : ICommand
    {
        readonly Stereo _stereo;


        public StereoOnWithCdCommand(Stereo stereo)
        {
            _stereo = stereo;
        }


        public void Execute()
        {
            _stereo.On();
            _stereo.SetCD();
        }

        public void Undo()
        {
            _stereo.Off();
        }
    }
}