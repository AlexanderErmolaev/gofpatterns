﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class HottubOffCommand : ICommand
    {
        readonly Hottub _hottub;


        public HottubOffCommand(Hottub hottub)
        {
            _hottub = hottub;
        }


        public void Execute()
        {
            _hottub.Off();
        }

        public void Undo()
        {
            _hottub.On();
        }
    }
}