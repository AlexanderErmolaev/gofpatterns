using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class CeilingFanLowCommand : ICommand
    {
        readonly CeilingFan _ceilingFan;


        public CeilingFanLowCommand(CeilingFan ceilingFan)
        {
            _ceilingFan = ceilingFan;
        }


        public void Execute()
        {
            _ceilingFan.SetSpeedToLow();
        }

        public void Undo()
        {
            _ceilingFan.Off();
        }
    }
}