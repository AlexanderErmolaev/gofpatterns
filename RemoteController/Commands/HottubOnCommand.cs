﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class HottubOnCommand : ICommand
    {
        readonly Hottub _hottub;


        public HottubOnCommand(Hottub hottub)
        {
            _hottub = hottub;
        }


        public void Execute()
        {
            _hottub.On();
        }

        public void Undo()
        {
            _hottub.Off();
        }
    }
}