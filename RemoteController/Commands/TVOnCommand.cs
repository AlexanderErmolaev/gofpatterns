﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class TvOnCommand : ICommand
    {
        readonly TV _tv;


        public TvOnCommand(TV tv)
        {
            _tv = tv;
        }


        public void Execute()
        {
            _tv.On();
        }

        public void Undo()
        {
            _tv.Off();
        }
    }
}