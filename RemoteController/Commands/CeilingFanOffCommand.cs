﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class CeilingFanOffCommand : ICommand
    {
        readonly CeilingFan _ceilingFan;
        private CeilingFanSpeed _ceilingFanPrevSpeed;


        public CeilingFanOffCommand(CeilingFan ceilingFan)
        {
            _ceilingFan = ceilingFan;
        }


        public void Execute()
        {
            _ceilingFanPrevSpeed = _ceilingFan.Speed;
            _ceilingFan.Off();
        }

        public void Undo()
        {
            switch (_ceilingFanPrevSpeed)
            {
                case CeilingFanSpeed.Off:
                    _ceilingFan.Off();
                    break;
                case CeilingFanSpeed.Low:
                    _ceilingFan.SetSpeedToLow();
                    break;
                case CeilingFanSpeed.Medium:
                    _ceilingFan.SetSpeedToMedium();
                    break;
                case CeilingFanSpeed.High:
                    _ceilingFan.SetSpeedToHigh();
                    break;
            }
        }
    }
}