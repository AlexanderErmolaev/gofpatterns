﻿using RemoteController.Devices;

namespace RemoteController.Commands
{
    public class GarageDoorCloseCommand : ICommand
    {
        readonly GarageDoor _garageDoor;


        public GarageDoorCloseCommand(GarageDoor door)
        {
            _garageDoor = door;
        }


        public void Execute()
        {
            _garageDoor.Close();
        }

        public void Undo()
        {
            _garageDoor.Open();
        }
    }
}