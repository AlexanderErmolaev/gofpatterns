﻿using System;
using System.Text;
using RemoteController.Commands;

namespace RemoteController
{
    public class RemoteControl
    {
        readonly ICommand[] _onCommands;
        readonly ICommand[] _offCommands;
        ICommand _undoCommand;


        public RemoteControl()
        {
            _onCommands = new ICommand[7];
            _offCommands = new ICommand[7];

            for (var i = 0; i < 7; i++)
            {
                _onCommands[i] = new NoCommand();
                _offCommands[i] = new NoCommand();
            }

            _undoCommand = new NoCommand();
        }


        public void SetCommand(int slot, ICommand onCommand, ICommand offCommand)
        {
            _onCommands[slot] = onCommand;
            _offCommands[slot] = offCommand;
        }

        public void OnButtonWasPushed(int slot)
        {
            _onCommands[slot].Execute();
            _undoCommand = _onCommands[slot];
        }

        public void OffButtonWasPushed(int slot)
        {
            _offCommands[slot].Execute();
            _undoCommand = _offCommands[slot];
        }

        public void UndoButtonWasPushed()
        {
            _undoCommand.Undo();
        }


        public override String ToString()
        {
            var stringBuilder = new StringBuilder("-------Remote control-------");
            for (var i = 0; i < _onCommands.Length; i++)
            {
                stringBuilder.AppendLine(String.Format("[slot {0}: {1} {2}]", i, _onCommands[i], _offCommands[i]));
            }

            return stringBuilder.ToString();
        }

    }
}
