using System;

namespace RemoteController.Devices
{
    public class CeilingFan
    {
        private readonly string _placeName;


        public CeilingFanSpeed Speed { get; set; }


        public CeilingFan(string placeName)
        {
            _placeName = placeName;
        }

        public void Off()
        {
            Speed = CeilingFanSpeed.Off;
            Console.WriteLine("Ceiling fan is off in {0}.", _placeName);
        }

        public void SetSpeedToLow()
        {
            Speed = CeilingFanSpeed.Low;
            Console.WriteLine("Ceiling fan speed is setted to {0}.", Speed);
        }

        public void SetSpeedToMedium()
        {
            Speed = CeilingFanSpeed.Medium;
            Console.WriteLine("Ceiling fan speed is setted to {0}.", Speed);
        }

        public void SetSpeedToHigh()
        {
            Speed = CeilingFanSpeed.High;
            Console.WriteLine("Ceiling fan speed is setted to {0}.", Speed);
        }
    }

    public enum CeilingFanSpeed
    {
        Off,
        Low,
        Medium,
        High
    }
}