using System;

namespace RemoteController.Devices
{
    public class GarageDoor
    {
        private readonly string _placeName;


        public GarageDoor(string placeName)
        {
            _placeName = placeName;
        }


        public void Open()
        {
            Console.WriteLine("Garage door is open in {0}.", _placeName);
        }

        public void Close()
        {
            Console.WriteLine("Garage door is close in {0}.", _placeName);
        }
    }
}