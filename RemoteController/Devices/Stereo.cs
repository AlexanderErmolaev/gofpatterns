using System;

namespace RemoteController.Devices
{
    public class Stereo
    {
        private readonly string _placeName;


        public StereoMode Mode { get; set; }


        public Stereo(string placeName)
        {
            _placeName = placeName;
        }

        public void On()
        {
            Mode = StereoMode.Radio;
            Console.WriteLine("Stereo is on in {0}.", _placeName);
        }

        public void Off()
        {
            Mode = StereoMode.Off;
            Console.WriteLine("Stereo is off in {0}.", _placeName);
        }

        public void SetCD()
        {
            Mode = StereoMode.CD;
            Console.WriteLine("Stereo CD is setted in {0}.", _placeName);
        }

        public void SetDVD()
        {
            Mode = StereoMode.DVD;
            Console.WriteLine("Stereo DVD is setted in {0}.", _placeName);
        }

        public void SetRadio()
        {
            Mode = StereoMode.Radio;
            Console.WriteLine("Stereo Radio is setted in {0}.", _placeName);
        }

        public void SetVolume(int volume)
        {
            Console.WriteLine("Stereo Radio is setted to {0} in {1}.", volume, _placeName);
        }
    }

    public enum StereoMode
    {
        Off,
        CD,
        DVD,
        Radio
    }
}