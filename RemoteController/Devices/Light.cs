﻿using System;

namespace RemoteController.Devices
{
    public class Light
    {
        private readonly string _placeName;


        public Light(string placeName)
        {
            _placeName = placeName;
        }


        public void On()
        {
            Console.WriteLine("Light is on in {0}.", _placeName);
        }

        public void Off()
        {
            Console.WriteLine("Light is off in {0}.", _placeName);
        }
    }
}
