﻿using System;

namespace RemoteController.Devices
{
    public class TV
    {
        private readonly string _placeName;


        public TV(string placeName)
        {
            _placeName = placeName;
        }


        public void On()
        {
            Console.WriteLine("TV is on in {0}.", _placeName);
        }

        public void Off()
        {
            Console.WriteLine("TV is off in {0}.", _placeName);
        }
    }
}
