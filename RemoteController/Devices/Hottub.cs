﻿using System;

namespace RemoteController.Devices
{
    public class Hottub
    {
        private readonly string _placeName;


        public Hottub(string placeName)
        {
            _placeName = placeName;
        }


        public void On()
        {
            Console.WriteLine("Hottub is on in {0}.", _placeName);
        }

        public void Off()
        {
            Console.WriteLine("Hottub is off in {0}.", _placeName);
        }
    }
}
