﻿namespace RemoteController
{
    public class MacroCommand : ICommand
    {
        readonly ICommand[] _commands; 


        public MacroCommand (ICommand[] сommands)
        {
            _commands = сommands; 
        }


        public void Execute()
        {
            foreach (var command in _commands)
            {
                command.Execute();
            }
        }

        public void Undo()
        {
            foreach (var command in _commands)
            {
                command.Undo();
            }
        }
    }
}
