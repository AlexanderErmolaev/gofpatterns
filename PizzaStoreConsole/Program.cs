﻿using System;

using PizzaStore;

namespace PizzaStoreConsole
{
    static class Program
    {
        static void Main()
        {
            PizzaStore.PizzaStore nYPizzaStore = new NyStylePizzaStore();
            PizzaStore.PizzaStore chicagoPizzaStore = new ChicagoStylePizzaStore();

            nYPizzaStore.OrderPizza("Cheese");

            chicagoPizzaStore.OrderPizza("Cheese");

            Console.ReadKey();
        }
    }
}