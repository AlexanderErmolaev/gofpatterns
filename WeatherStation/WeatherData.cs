﻿using System.Collections.Generic;

namespace WeatherStation
{
    public class WeatherData : ISubject
    {
        private readonly List<IObserver> _observers;
        private double _temperature;
        private double _humidity;
        private double _pressure;


        public WeatherData()
        {
            _observers = new List<IObserver>();
        }


        public void RegisterObserver(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void NotifyObserver()
        {
            foreach (var observer in _observers)
            {
                observer.Update(_temperature, _humidity, _pressure);
            }
        }

        public double GetTemperature()
        {
            return _temperature;
        }

        public double GetHumidity()
        {
            return _humidity;
        }

        public double GetPressure()
        {
            return _pressure;
        }

        public void SetMeasurements(float temperature, float humidity, float pressure)
        {
            _temperature = temperature;
            _humidity = humidity;
            _pressure = pressure;

            MeasurementsChanged();
        }


        private void MeasurementsChanged()
        {
            NotifyObserver();
        }
    }
}