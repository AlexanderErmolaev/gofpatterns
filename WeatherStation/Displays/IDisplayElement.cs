﻿namespace WeatherStation.Displays
{
    public interface IDisplayElement
    {
        void Display();
    }
}