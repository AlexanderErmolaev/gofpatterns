﻿namespace WeatherStation.Displays
{
    public class CurrentConditionsDisplay : IDisplayElement, IObserver
    {
        private double _temperature;
        private double _humidity;


        public CurrentConditionsDisplay(ISubject weatherData)
        {
            weatherData.RegisterObserver(this);
        }


        public void Update(double temperature, double humidity, double pressure)
        {
            _temperature = temperature;
            _humidity = humidity;

            Display();
        }

        public void Display()
        {
            System.Console.WriteLine("Current conditions: {0}F degrees and {1}% humidity.", _temperature, _humidity);
        }
    }
}