﻿namespace WeatherStation.Displays
{
    public class ForecastDisplay : IDisplayElement, IObserver
    {
        private double _temperature;
        private double _humidity;


        public ForecastDisplay(ISubject weatherData)
        {
            weatherData.RegisterObserver(this);
        }

        public void Update(double temperature, double humidity, double pressure)
        {
            _temperature = temperature;
            _humidity = humidity;

            Display();
        }

        public void Display()
        {
            System.Console.WriteLine("Current forecas: {0}F degrees and {1}% humidity.", _temperature, _humidity);
        }
    }
}