﻿namespace Chocoholic
{
    public class ChocolateBoiler
    {
        private bool _isEmpty;
        private bool _isBoiled;

        private static volatile ChocolateBoiler _chocolateBoilerInstance;
        private static readonly object SyncObject = new object();


        public static ChocolateBoiler Instance
        {
            get
            {
                if (_chocolateBoilerInstance == null)
                {
                    lock (SyncObject)
                    {
                        if (_chocolateBoilerInstance == null)
                        {
                            _chocolateBoilerInstance = new ChocolateBoiler();
                        }
                    }
                }

                return _chocolateBoilerInstance;
            }
        }


        private ChocolateBoiler()
        {
            _isEmpty = true;
            _isBoiled = false;
        }


        public void Fill()
        {
            if (_isEmpty)
            {
                _isEmpty = false;
                _isBoiled = false;
            }
        }

        public void Drain()
        {
            if (!_isEmpty && _isBoiled)
            {
                _isEmpty = true;
            }
        }

        public void Boil()
        {
            if (!_isEmpty && !_isBoiled)
            {
                _isBoiled = true;
            }
        }
    }
}