﻿using System;
using RemoteController;
using RemoteController.Commands;
using RemoteController.Devices;

namespace RemoteControllerConsole
{
    static class Program
    {
        static void Main()
        {
            var remoteControl = new RemoteControl();

            var livingRoomLight = new Light("Living Room");
            var kitchenLight = new Light("Kitchen");
            var ceilingFan = new CeilingFan(String.Empty);
            var garageDoor = new GarageDoor(String.Empty);
            var stereo = new Stereo(String.Empty);
            var tv = new TV(String.Empty);
            var hottub = new Hottub(String.Empty);

            var livingRoomLightOn = new LightOnCommand(livingRoomLight);
            var livingRoomLightOff = new LightOffCommand(livingRoomLight);

            var kitchenLightOn = new LightOnCommand(kitchenLight);
            var kitchenLightOff = new LightOffCommand(kitchenLight);

            var ceilingFanLow = new CeilingFanLowCommand(ceilingFan);
            var ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
            var ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);
            var ceilingFanOff = new CeilingFanOffCommand(ceilingFan);

            var garageDoorOpen = new GarageDoorOpenCommand(garageDoor);
            var garageDoorClose = new GarageDoorCloseCommand(garageDoor);

            var stereoOnWithCd = new StereoOnWithCdCommand(stereo);
            var stereoOff = new StereoOffCommand(stereo);

            var tvOn = new TvOnCommand(tv);
            var tvOff = new TvOffCommand(tv);

            var hottubOn = new HottubOnCommand(hottub);
            var hottubOff = new HottubOffCommand(hottub);

            remoteControl.SetCommand(0, livingRoomLightOn, livingRoomLightOff);
            remoteControl.SetCommand(1, kitchenLightOn, kitchenLightOff);
            remoteControl.SetCommand(2, garageDoorOpen, garageDoorClose);

            remoteControl.SetCommand(3, ceilingFanLow, ceilingFanOff);
            remoteControl.SetCommand(4, ceilingFanMedium, ceilingFanOff);
            remoteControl.SetCommand(5, ceilingFanHigh, ceilingFanOff);

            remoteControl.SetCommand(6, stereoOnWithCd, stereoOff);


            Console.WriteLine(remoteControl);


            remoteControl.OnButtonWasPushed(0);
            remoteControl.OffButtonWasPushed(0);

            remoteControl.OnButtonWasPushed(1);
            remoteControl.OffButtonWasPushed(1);

            remoteControl.OnButtonWasPushed(2);
            remoteControl.OffButtonWasPushed(2);

            remoteControl.OnButtonWasPushed(3);
            remoteControl.OffButtonWasPushed(3);
            remoteControl.UndoButtonWasPushed();

            remoteControl.OnButtonWasPushed(5);
            remoteControl.OnButtonWasPushed(4);
            remoteControl.UndoButtonWasPushed();

            ICommand[] partyOn = { livingRoomLightOn, stereoOnWithCd, tvOn, hottubOn };
            ICommand[] partyOff = { livingRoomLightOff, stereoOff, tvOff, hottubOff };

            var partyOnMacro = new MacroCommand(partyOn);
            var partyOffMacro = new MacroCommand(partyOff);

            remoteControl.SetCommand(0, partyOnMacro, partyOffMacro);

            Console.WriteLine(remoteControl);


            remoteControl.OnButtonWasPushed(0);
            remoteControl.OffButtonWasPushed(0);

            Console.ReadKey();
        }
    }
}